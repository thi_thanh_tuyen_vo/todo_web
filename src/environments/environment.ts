// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCVlTrXJI3twP1ozhXthuKdOfUhFPE0kvg",
    authDomain: "to-do-f8e7d.firebaseapp.com",
    databaseURL: "https://to-do-f8e7d.firebaseio.com",
    projectId: "to-do-f8e7d",
    storageBucket: "to-do-f8e7d.appspot.com",
    messagingSenderId: "45693920253",
    appId: "1:45693920253:web:57759c274ac77d5c2c6f94",
    measurementId: "G-7WXSS74J7R"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related notfound stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an notfound is thrown.
 */
// import 'zone.js/dist/zone-notfound';  // Included with Angular CLI.
