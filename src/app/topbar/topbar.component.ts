import { Component, OnInit , Input} from '@angular/core';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  @Input() isdashboard:string;
  @Input() ishomepage:string;

  user;
  photoURL;
  name;
  userdata;
  constructor(public authservice:AuthService) {
    this.userdata = authservice.userData;
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.userdata = this.authservice.userData;
    this.photoURL = this.userdata.photoURL;
    this.name = this.user.displayName;
  }

  logout() {
    this.authservice.signOut();
    }

}
