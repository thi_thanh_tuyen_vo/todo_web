/**
 * Décrit les partie de l'application qui s'emboitent (modules)
 */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';

// components
import { MaincontentComponent , DialogOverviewExampleDialog } from './maincontent/maincontent.component';
import { AddnewtaskComponent } from './addnewtask/addnewtask.component';
import { LoginComponent } from './login/login.component';

//service
import { AuthService } from "./services/auth.service";

// for two way binding and forms
import { FormsModule , ReactiveFormsModule, FormGroupDirective} from '@angular/forms';

// firebase
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireAuthModule } from "@angular/fire/compat/auth";
import { environment } from '../environments/environment';

// icons, Animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// toaster (notification)
import { ToastrModule } from 'ngx-toastr';

// For MDB Angular
import { MDBBootstrapModule } from 'angular-bootstrap-md';

// routing
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';

// social login + database
import * as firebase from 'firebase/app';
import { FbserviceService } from './services/fbservice.service';

// Component
import { FooterComponent } from './footer/footer.component';
import { EmailVerifComponent } from './email-verif/email-verif.component';
import { PasswordForgetComponent } from './password-forget/password-forget.component';
import { TopbarComponent } from './topbar/topbar.component';
import {NotfoundComponent} from './notfound/notfound.component';

// Angular Material
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatTabsModule} from "@angular/material/tabs";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDialogModule} from "@angular/material/dialog";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";

// Authentification access
import {AuthGuard} from "./services/auth-guard.service";

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    MaincontentComponent,
    DialogOverviewExampleDialog,
    AddnewtaskComponent,
    LoginComponent,
    FooterComponent,
    EmailVerifComponent,
    PasswordForgetComponent,
    TopbarComponent,
    NotfoundComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FontAwesomeModule,
    ReactiveFormsModule,
    DatePickerModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    MDBBootstrapModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTabsModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDatepickerModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatInputModule,
    MatButtonModule
  ],

  entryComponents:[MaincontentComponent,DialogOverviewExampleDialog],
  providers: [ FbserviceService,
    AngularFireAuthModule,
    AuthService,
    AuthGuard
],

  bootstrap: [AppComponent]
})

export class AppModule { }
