import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user;
  login = true;
  constructor(public authService: AuthService , private router:Router) { }

  ngOnInit() {
  }

  // Dés qu'on est logger on arrive sur la page todolistapp
  submit(values){
    console.log(values.value);
    this.router.navigate(['/todolistapp']);
  }

  register(){
    this.login= !this.login;
  }


}
