import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormGroupDirective, NgForm, FormBuilder } from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


import { Observable } from 'rxjs';
import { task ,colors ,statusarray , categoryarray } from '../module';

import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FbserviceService } from '../services/fbservice.service';
import { DialogOverviewExampleDialog } from '../maincontent/maincontent.component';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addnewtask',
  templateUrl: './addnewtask.component.html',
  styleUrls: ['./addnewtask.component.css']
})

export class AddnewtaskComponent implements OnInit {

  lists:Observable<task[]>;

  extrafield;
  newItem:task;

  statusarray = [ "Terminée","À terminer" ];
  categoryarray = ["Maison","Travail","Etude"];
  priorityarray = [ "Elevée","Moyenne","Faible" ];

  titlevalue;
  categoryvalue;
  priorityvalue;
  myform: FormGroup;

  // dialogue category
  newcategory;
  resultcatlist;

  constructor(private service:FbserviceService,private formBuilder: FormBuilder,public toastr:ToastrService) { }

  ngOnInit() {
    this.lists = this.service.getLists();

    // Formulaire de saisie d'une tâche
    this.myform = this.formBuilder.group({
      'title': new FormControl("",Validators.required),
      'category': new FormControl("",Validators.required),
      'priority': new FormControl("",Validators.required),
      'description': new FormControl("",Validators.required),
      'begindate': new FormControl("",Validators.required),
      'enddate': new FormControl("",Validators.required),
      'status': new FormControl("",Validators.required),
    });
  }

  // Affiche le formulaire
  showform(extrafield , title) {
    this.extrafield = extrafield;
    extrafield.style.display = "block";
  }

  // Masque le formulaire
  remvform(extrafield,event) {
    if(event.path[0].className == "container my-4"){
      extrafield.style.display = "none";
    }
  }

  // Recupère titre d'une tâche et le stocke
  get title(){
    this.titlevalue = this.myform.get('title').value;
    return this.myform.get('title');
  }

  // Recupére catégorie d'une tâche
  get category(){
    return this.myform.get('category').value;
  }

  get priority(){
    return this.myform.get('priority');
  }

  get description(){
    return this.myform.get('description');
  }

  get begindate(){
    return this.myform.get('begindate');
  }

  get enddate(){
    return this.myform.get('enddate');
  }

  get status(){
    return this.myform.get('status');
  }

  // Ajoute une tache si le formulaire est valide
  addtask(data,f){
    console.log(data);
    this.service.addData(data);

    if (this.myform.invalid) {
      return;
    }

    this.extrafield.style.display = "none";
  }

  // Permet de stocker les états du formulaire
  // et renvoie l'état actuel du formulaire
  private currentFormState() {
    const formState = {
      dirty: this.myform.dirty,
      invalid: this.myform.invalid,
      pending: this.myform.pending,
      pristine: this.myform.pristine,
      status: this.myform.status,
      touched: this.myform.touched,
      untouched: this.myform.untouched,
      valid: this.myform.valid
    };

    return formState;
  }

  // Notification
  showToaster(msg){
    this.toastr.success(msg);

  }
}
