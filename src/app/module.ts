/**
 * Caractéristique d'une card qui sont communes à tous les composants
 */

//Défini les propriétés d'une card
export interface task {
    id?:string;
    title?:string;
    category?:string;
    description?:string;
    priority?:string;
    begindate?:Date;
    enddate?:Date;
    status?:string;
  };

// Couleurs des cards (à choisir)
  export var colors = ["#f28b82", "#ffde7e" ,"fff475","#ccff90", "#a7ffeb","#cbf0f8","#aecbfa","#aecbfa","#d7aefb","#fdcfe8","#e6c9a8","#e8eaed","#fff" ];

// Tableau des status des tâches
  export var statusarray = [
  "Terminée","À terminée"
  ];

// Tableau des catégories des tâches
  export var categoryarray = [
    "Maison","Travail","Etude"
  ];

// Tableau des priorités des tâches
   export var priorityarray = [
    "Elevée","Moyenne","Faible"
  ];



