/**
 * Définie les routes de l'application
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule  } from '@angular/router';

//components
import { MaincontentComponent , DialogOverviewExampleDialog } from './maincontent/maincontent.component';
import { AddnewtaskComponent } from './addnewtask/addnewtask.component';
import { LoginComponent } from './login/login.component';
import { EmailVerifComponent } from './email-verif/email-verif.component';
import { PasswordForgetComponent } from './password-forget/password-forget.component';
import { TopbarComponent } from './topbar/topbar.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AuthGuard } from "./services/auth-guard.service";

const routes: Routes = [
      {path: '', component: LoginComponent },
      {path:'login',component:LoginComponent},
      {path:'email-verif',component:EmailVerifComponent},
      {path:'password-forget',component:PasswordForgetComponent},
      {path: 'todolistapp', canActivate: [AuthGuard], component:MaincontentComponent },
      {path: 'topbar', canActivate: [AuthGuard], component: TopbarComponent },
      {path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

