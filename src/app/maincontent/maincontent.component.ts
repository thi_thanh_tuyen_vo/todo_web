import { Component, OnInit , Inject } from '@angular/core';
import { FbserviceService } from '../services/fbservice.service';
import { Observable } from 'rxjs';
import {task, colors, statusarray, priorityarray, categoryarray} from '../module';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { trigger, state, transition, animate, style } from '@angular/animations';
import { Router } from "@angular/router";

export interface DialogData {
  [x: string]: any;
  title:string,
  category:string,
  priority:string,
  description:string
}

@Component({
  selector: 'app-maincontent',
  templateUrl: './maincontent.component.html',
  styleUrls: ['./maincontent.component.css'],
  animations:[
    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateY(1%)'})),
      transition(':enter', [
        style({ transform: 'translateY(-17%)' }),
        animate('2.5s 300ms ease-in')
      ]),

    ]),
    trigger('EnterLeave', [
      state('flyInlist', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('3.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]

})
export class MaincontentComponent implements OnInit {
  data;
  updateddata;
  resultData;
  cardcolor;
  toggleview = false;
  editvalues;
  filtered = new Array();
  searched = new Array();
  searchvalue;

  statusarray = [ "Terminée","À terminer" ];
  categoryarray = [ "Maison","Travail","Etude" ];
  priorityarray = [ "Elevée","Moyenne","Faible" ];


  lists:Observable<task[]>;

  title;
  category;
  priority;
  description;
  begindate;
  enddate;
  status;
  chngColor = 0;

  filtervalue;
  icontoggle=false;
  iconvalue = "format_list_bulleted";

  // animation
  state:string = "";

  private newItem: task;

  constructor(private service:FbserviceService , public dialog: MatDialog,private toastr: ToastrService, public router: Router) { }

  ngOnInit() {

    let Todolist = {
      title: this.title,
      category: this.category,
      priority: this.priority,
      description: this.description,
      begindate: this.begindate,
      enddate: this.enddate,
      status: this.status,
    };

    this.service.getLists().subscribe(result => {
      console.log(result);
      this.resultData = result;
      for(let a of this.resultData) {
      a.begindate = a.begindate.toDate();
      a.begindate = a.begindate.toISOString().split('T')[0];
      a.enddate = a.enddate.toDate();
      a.enddate = a.enddate.toISOString().split('T')[0];
      }
      this.filtered = this.resultData;
  });
  }

  // Vide le formulaire
  remvform(extrafield) {
    extrafield.style.display = "none";
  }


  fbadditem() {
    console.log(this.title);
    if(this.title && this.category && this.priority && this.description && this.begindate && this.enddate && this.status) {
    this.newItem.title = this.title;
    this.newItem.category = this.category;
    this.newItem.priority = this.priority;
    this.newItem.description = this.description;
    this.newItem.begindate = this.begindate;
    this.newItem.enddate = this.enddate;
    this.newItem.status = this.status;

    this.service.addData(this.newItem);

    this.title = "";
    this.category="";
    this.priority="";
    this.description="";
    this.begindate = "";
    this.enddate="";
    this.status="";

  } else {
    console.log("Entrer des données");
  }
  }*

  // Supprime une tâche
  fbdeleteItem(item){
    this.service.deleteData(item);
    this.service.fbupdateData(item);
  }

  // Change la couleur des cards
  chngcolor(editvalues) {
    if(this.chngColor == 12){
      this.chngColor =0;
    } else {
      console.log(editvalues);
      editvalues.style.backgroundColor = colors[this.chngColor];
    }
    this.chngColor++;
  }

  // CHange la vue des cards
  tblview(itemdiv ,icon) {
    if (this.icontoggle == false) {
      this.iconvalue = "profile";
      icon.innerHTML = "profile";
      itemdiv.style.columnCount = "1";

    } else {
      this.iconvalue = "format_list_bulleted";
      itemdiv.style.columnCount = "3";
      icon.innerHTML = "format_list_bulleted";
    }
    this.icontoggle = !this.icontoggle;
  }

  // PErmet le filtrage des cards
  onChange(value){
    console.log(value.value.length);
    if(value.value.length > 0){
      this.filtered = [];
      var filterproerties = value.value;
      var allcategory = new Array();
      var allpriority = new Array();
      var allstatus= new Array();
      var sameid= new Array();


      for(let onedata in this.resultData){
         var  fbcategory = this.resultData[onedata].category;
         var  fbpriority = this.resultData[onedata].priority;
         var  fbstatus = this.resultData[onedata].status;
         var  fid = this.resultData[onedata].id;
          for(let a in filterproerties){
              if(filterproerties[a]== fbcategory || filterproerties[a]== fbpriority || filterproerties[a] == fbstatus){
              for(let b in this.filtered){
              if(this.filtered[b].id == fid) {
                  console.log("same id " + fid +" " + this.filtered[b].id);
                  this.filtered.pop();
              } else {
                  sameid.push(fid);
              }
          }
                this.filtered.push(this.resultData[onedata]);

            }
          }
      }
    } else {
      this.filtered = this.resultData;
    }
}

  // Recherche de tâche
  research(event){
    this.searched = [];
    let searchidlist = new Array;
    if(event.keyCode == 13) {
      for(let k=0; k < this.filtered.length; k++) {
        let list = this.filtered[k];
        let title = list.title;
        let category = list.category;
        let priority = list.priority;
        let description = list.description;
        let status = list.status;

        if(title.includes(this.searchvalue) || category.includes(this.searchvalue) || priority.includes(this.searchvalue) || description.includes(this.searchvalue)|| status.includes(this.searchvalue) ){
          this.searched.push(list);
        }
      }
      this.filtered= this.searched;
    }

    if(!this.searchvalue){
      this.filtered = this.resultData;
    }
  }
    filtertag = new FormControl();

    showToaster(msg){
      this.toastr.success(msg);
  }

  // Affiche la card (NE FONCTIONNE PAS)
  openDialog(item:task): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '40%',
      data: {
        id:item.id,
        title: item.title,
        category: item.category,
        priority:item.priority,
        description:item.description,
        begindate:item.begindate,
        enddate:item.enddate,
        status:item.status
        }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.updateddata = result;
      console.log('Fenêtre fermée' + this.updateddata.title);
    });
  }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})

/**
 * À CORRIGER (Pas d'affichage de la fenêtre des taches à modifier)
 */
export class DialogOverviewExampleDialog implements OnInit {

  category = this.data.category;
  statusarray = [ "Terminée","À terminer" ];
  categoryarray = new Array();
  priorityarray = [ "Elevée","Moyenne","Faible" ];

  ngOnInit(){
  }

  allValues = this.categoryarray;
  valueOptions = Object.keys(categoryarray);

  pallValues = priorityarray;
  pvalueOptions = Object.keys(priorityarray);

  sallValues = statusarray;
  svalueOptions = Object.keys(statusarray);


  bdate = this.data.begindate;
  edate = this.data.enddate;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private service:FbserviceService,db:AngularFirestore, private toastr: ToastrService) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // Modifie une tâche
  dateupdate(item:task) {
    console.log("item "+ item.id);
    let newbegindate = new Date(this.bdate);
    let newenddate = new Date(this.edate);
    item.begindate = newbegindate;
    item.enddate = newenddate;
    this.service.fbupdateData(item);
  }

  showToaster(msg){
    this.toastr.success(msg);
  }

}
