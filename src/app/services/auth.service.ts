import { Injectable, NgZone } from '@angular/core';
import { User } from "./user";

import { AngularFireAuth } from "@angular/fire/compat/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Router } from "@angular/router";
import firebase from 'firebase/compat/app';

/**
 * Gère l'authentification de l'utilisateur
 */

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  // Permet de stocker les données utilisateur
  userData: any;
  userUid;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone // Supprime l'avertissement hors champ
  ) {

    /* Sauvegarde les données utilisateur en local lors de sa
    connexion (si il possède un compte) */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        this.userUid = user.uid;
        console.log("uid" + this.userUid);
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    })
  }

  /** Connexion avec email et mot de passe
   * Vérifie la connexion de l'utilisateur et
   * renvoie la promesse qui consiste à renvoyer les données
   * et à rediriger vers la page de la route todolistapp
   * @param email
   * @param password
   * @constructor
   */
  SignIn(email, password) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['todolistapp']);
        });
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  /** S'inscrire avec email et mot de passe
   * Créer un nouvel utilisateur et renvoie sa promesse
   * @param email
   * @param password
   * @constructor
   */
  SignUp(email, password) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        /* on appel la fonction SendVerificaitonMail() quand l'utilisateur s'inscrit
         et on renvoie la promesse */
        this.SendVerificationMail();
        // Stocke info user dans une fonction
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // Envoie d'un mail de confirmation quand l'utilisateur s'inscrit
  SendVerificationMail() {
    return this.afAuth.currentUser.then(u => u.sendEmailVerification())
    .then(() => {
      this.router.navigate(['email-verif']);
    })
  }

  // Retrouver son mot de passe
  ForgotPassword(passwordResetEmail) {
    return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
    .then(() => {
      window.alert('Un email de ré-initialisation de mot de passe à été envoyé, Veuillez vérifier votre boite mail');
      this.router.navigate(['password-forget']);
    }).catch((error) => {
      window.alert(error)
    })
  }

  // Retourne vrai lorsque l'utilisateur est enregistré et que le mail est confirmé
  // (utilisateur enregistré en BDD)
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null ) ? true : false;
  }

  // S'enregistrer avec google
  GoogleAuth() {
    return this.AuthLogin(new firebase.auth.GoogleAuthProvider());
  }


  // S'enregistrer avec Facebook
  FacebookAuth(){
    return this.AuthLogin(new firebase.auth.FacebookAuthProvider());
 }

  // Logique des fenêtres d'authentification google, fb, twitter...
  AuthLogin(provider) {
    return this.afAuth.signInWithPopup(provider)
    .then((result) => {
       this.ngZone.run(() => {
          this.router.navigate(['todolistapp']);
        })
      this.SetUserData(result.user);
    }).catch((error) => {
      window.alert(error)
    })
  }

  /* Configuration des données utilisateur lors de la connexion avec nom d'utilisateur/mot de passe,
  l'inscription avec nom d'utilisateur/mot de passe et l'inscription avec les réseaux sociaux
   dans la base de données Firestore en utilisant AngularFirestore + le service AngularFirestoreDocument */
  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      userid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  // Déconnexion
  signOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    })
  }

}
