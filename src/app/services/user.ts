// Information de l'utilisateur
export interface User {
   userid: string;
   email: string;
   displayName: string;
   photoURL: string;
   emailVerified: boolean;
}
