import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { task } from '../module';
import { AuthService } from './auth.service';
import firebase from "firebase/compat/app";
import { doc, deleteDoc } from "firebase/firestore";
import firestore = firebase.firestore;

/**
 * Gère les appel à la base de donnée FireStore
 */

@Injectable({
  providedIn: 'root'
})

export class FbserviceService {
  listCollection:AngularFirestoreCollection;
  listDocument:AngularFirestoreDocument;
  userdata;
  currentuid;
  list;
  catlist;

  constructor(private db:AngularFirestore, private authservice:AuthService) {
    this.userdata = authservice.userData;
    this.currentuid = authservice.userUid;
    this.listCollection = this.db.collection('users');
    console.log(this.userdata);
    this.currentuid = this.userdata.uid;
  }

  // Récupère des donnée de la BDR
  getLists() {
    var fbdata =  this.listCollection.doc(this.currentuid).collection(this.currentuid).valueChanges();
    return fbdata;
  }

  // Ajoute des données à la BDR
  addData(newItem:task){
      const id = this.db.createId();
      console.log("Demande de création" + this.currentuid);
      newItem.id = id;
      this.listCollection.doc(this.currentuid).collection(this.currentuid).doc(id).set(Object.assign({}, newItem));

      console.log(newItem);
    }

    // Supprime une donnée de la BDD
    deleteData(item){
      this.listDocument = this.listCollection.doc(this.currentuid).collection(this.currentuid).doc(item.id);
      this.listDocument = this.db.doc(`this.userdata.userid/${item.id}`);
      this.listDocument.delete();
    }

    // MEt à jour une donnée de la BDR
    fbupdateData(updateddata:task){
      console.log(updateddata.id);
      this.listDocument = this.listCollection.doc(this.currentuid).collection(this.currentuid).doc(updateddata.id);
      this.listDocument.update(updateddata);
    }

  }
